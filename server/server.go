package main

import (
	"bytes"
	"log"
	"net/http"

	"github.com/gorilla/websocket"
)

type Server struct {
	publishers  map[*Publisher]bool
	subscribers map[*Subscriber]bool

	regPublisher    chan *Publisher
	regSubscriber   chan *Subscriber
	unregPublisher  chan *Publisher
	unregSubscriber chan *Subscriber

	broadcast       chan []byte

	server *http.Server
}

var upgrader = websocket.Upgrader{}

func NewServer(addr string) *Server {

	return &Server{
		publishers:      make(map[*Publisher]bool),
		subscribers:     make(map[*Subscriber]bool),
		regPublisher:    make(chan *Publisher),
		regSubscriber:   make(chan *Subscriber),
		unregPublisher:  make(chan *Publisher),
		unregSubscriber: make(chan *Subscriber),
		broadcast:       make(chan []byte, 256),
		server: &http.Server{
			Addr: addr,
		},
	}
}

func (s *Server) publisherHandler(w http.ResponseWriter, r *http.Request) {
	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Printf("[ERROR] can't upgrade: %v", err)
		return
	}

	pub := NewPublisher(conn, s.broadcast, s.unregPublisher)
	s.regPublisher <- pub
	go pub.Listen()
}

func (s *Server) subscriberHandler(w http.ResponseWriter, r *http.Request) {
	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Printf("[ERROR] can't upgrade: %v", err)
		return
	}

	sub := NewSubscriber(conn, s.unregSubscriber)
	s.regSubscriber <- sub
	go sub.Listen()
}

func (s *Server) Run() error {

	go func() {
		for {
			select {
			case msg := <-s.broadcast:
				msg = bytes.Replace(msg, []byte("!"), []byte("?"), -1)
				for sub := range s.subscribers {
					sub.send <- msg
				}
			case pub := <-s.regPublisher:
				s.publishers[pub] = true
				s.setBroadcasting()
				log.Println("[DEBUG] publisher registered")
			case sub := <-s.regSubscriber:
				s.subscribers[sub] = true
				s.setBroadcasting()
				log.Println("[DEBUG] subscriber registered")
			case pub := <-s.unregPublisher:
				if _, ok := s.publishers[pub]; ok {
					delete(s.publishers, pub)
				}
				s.setBroadcasting()
				log.Println("[DEBUG] publisher unregistered")
			case sub := <-s.unregSubscriber:
				if _, ok := s.subscribers[sub]; ok {
					delete(s.subscribers, sub)
					close(sub.send)
				}
				s.setBroadcasting()
				log.Println("[DEBUG] subscriber unregistered")

			}
		}
	}()

	mux := http.NewServeMux()
	mux.HandleFunc("/publisher", s.publisherHandler)
	mux.HandleFunc("/subscriber", s.subscriberHandler)
	s.server.Handler = mux

	log.Printf("[INFO] server is listening on: %s", s.server.Addr)
	return s.server.ListenAndServe()
}


func (s *Server) setBroadcasting() {
	if len(s.subscribers) == 0 {
		for pub := range s.publishers {
			pub.Stop()
		}
		return
	}

	for pub := range s.publishers {
		pub.Start()
	}
}