package main

import (
	"flag"
	"fmt"
	"log"
	"os"

	"github.com/hashicorp/logutils"
)

var (
	port = flag.String("port", "8080", "http service port")
	dbg  = flag.Bool("debug", false, "debug mode")
)

func main() {
	flag.Parse()

	setupLog(*dbg)

	addr := fmt.Sprintf(":%s", *port)
	server := NewServer(addr)
	log.Fatal(server.Run())
}

func setupLog(dbg bool) {
	filter := &logutils.LevelFilter{
		Levels:   []logutils.LogLevel{"DEBUG", "INFO", "WARN", "ERROR"},
		MinLevel: logutils.LogLevel("INFO"),
		Writer:   os.Stdout,
	}

	log.SetFlags(log.Ldate | log.Ltime)

	if dbg {
		log.SetFlags(log.Ldate | log.Ltime | log.Lmicroseconds | log.Lshortfile)
		filter.MinLevel = logutils.LogLevel("DEBUG")
	}
	log.SetOutput(filter)
}
