package main

import (
	"log"
	"time"

	"github.com/gorilla/websocket"
)

type Subscriber struct {
	conn *websocket.Conn

	send       chan []byte
	unregister chan<- *Subscriber
}

const (
	// Time allowed to write a message to the peer.
	writeWait = 10 * time.Second
)

func NewSubscriber(conn *websocket.Conn, unregister chan<- *Subscriber) *Subscriber {
	return &Subscriber{
		conn:       conn,
		send:       make(chan []byte, 256),
		unregister: unregister,
	}
}

func (s *Subscriber) Listen() {
	defer func() {
		s.unregister <- s
		s.conn.Close()
	}()
	for {
		select {
		case msg, ok := <-s.send:
			s.conn.SetWriteDeadline(time.Now().Add(writeWait))
			if !ok {
				// The server closed the channel.
				s.conn.WriteMessage(websocket.CloseMessage, []byte{})
				return
			}

			w, err := s.conn.NextWriter(websocket.TextMessage)
			if err != nil {
				return
			}
			log.Printf("[DEBUG] send message (%s) to subscriber\n", msg)
			w.Write(msg)

			// Add queued chat messages to the current websocket message.
			n := len(s.send)
			for i := 0; i < n; i++ {
				w.Write([]byte{'\n'})
				msg := <-s.send
				log.Printf("[DEBUG] send message (%s) to subscriber\n", msg)
				w.Write(msg)
			}

			if err := w.Close(); err != nil {
				return
			}
		}
	}
}
