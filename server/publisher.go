package main

import (
	"log"

	"github.com/gorilla/websocket"
)

type Publisher struct {
	conn *websocket.Conn

	isBroadcasting bool

	broadcast  chan<- []byte
	unregister chan<- *Publisher
}

func NewPublisher(conn *websocket.Conn, broadcast chan<- []byte, unregister chan<- *Publisher) *Publisher {
	return &Publisher{
		conn:       conn,
		broadcast:  broadcast,
		unregister: unregister,
	}
}

func (p *Publisher) Listen() {
	defer func() {
		p.unregister <- p
		p.conn.Close()
	}()
	for {
		_, msg, err := p.conn.ReadMessage()
		if err != nil {
			if websocket.IsUnexpectedCloseError(err, websocket.CloseGoingAway, websocket.CloseAbnormalClosure) {
				log.Printf("[ERROR] can't read message from publisher: %v\n", err)
			}
			break
		}
		log.Printf("[DEBUG] get message (%s) from publisher\n", msg)
		p.broadcast <- msg
	}
}

func (p *Publisher) Start() {
	if p.isBroadcasting {
		return
	}

	if p.send("START") {
		p.isBroadcasting = true
	}
}

func (p *Publisher) Stop() {
	if !p.isBroadcasting {
		return
	}

	if p.send("STOP") {
		p.isBroadcasting = false
	}
}

func (p *Publisher) send(msg string) bool {
	log.Printf("[DEBUG] send message (%s) to publisher\n", msg)
	err := p.conn.WriteMessage(websocket.TextMessage, []byte(msg))
	if err != nil {
		log.Printf("[ERROR] can't send  message (%s) to publisher: %v\n", msg, err)
		return false
	}

	return true
}
