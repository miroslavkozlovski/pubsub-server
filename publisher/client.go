package main

import (
	"github.com/gorilla/websocket"
	"log"
	"net/url"
	"time"
)

type Client struct {
	addr string
	conn *websocket.Conn

	start chan struct{}
	stop chan struct{}
	close chan struct{}
}

func NewClient(addr string) *Client {
	return &Client{
		addr: addr,
		start: make(chan struct{}),
		stop: make(chan struct{}),
		close: make(chan struct{}),
	}
}

func (c *Client) Connect() {
	u := url.URL{Scheme: "ws", Host: *addr, Path: "/publisher"}

	var err error
	c.conn, _, err = websocket.DefaultDialer.Dial(u.String(), nil)
	if err != nil {
		log.Fatalf("[ERROR] can't get connection: %v", err)
	}
	defer c.conn.Close()

	log.Printf("[INFO] connected to server (%s)", u.String())

	go c.listen()

	for {
		select {
		case <-c.start:
			go c.send()
		case <-c.close:
			log.Println("[WARN] connection lost")
			return
		}
	}
}

func (c *Client) listen() {
	defer func() { c.close <- struct{}{} }()
	for {
		_, msg, err := c.conn.ReadMessage()
		if err != nil {
			if websocket.IsUnexpectedCloseError(err, websocket.CloseGoingAway, websocket.CloseAbnormalClosure) {
				log.Printf("[ERROR] can't read message from publisher: %v\n", err)
			}
			break
		}
		log.Printf("[DEBUG] get message (%s) from server\n", msg)

		switch string(msg) {
		case "START":
			c.start <- struct{}{}
		case "STOP":
			c.stop <- struct{}{}
		}
	}
}

func (c *Client) send() {
	for range time.Tick(time.Second) {
		select {
		case <-c.stop:
			return
		default:

		}

		msg := "as!jkdlf!asjd !"
		err := c.conn.WriteMessage(websocket.TextMessage, []byte(msg))
		if err != nil {
			log.Printf("[ERROR] can't send message: %v\n", err)
			c.close <- struct{}{}
			break
		}
		log.Println("[DEBUG] send message", msg)
	}
}