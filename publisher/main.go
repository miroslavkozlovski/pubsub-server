package main

import (
	"flag"
	"github.com/hashicorp/logutils"
	"log"
	"os"
)

var (
	addr = flag.String("addr", ":8080", "server address")
	dbg  = flag.Bool("debug", false, "debug mode")
)

func main() {
	flag.Parse()

	setupLog(*dbg)

	client := NewClient(*addr)
	client.Connect()
}

func setupLog(dbg bool) {
	filter := &logutils.LevelFilter{
		Levels:   []logutils.LogLevel{"DEBUG", "INFO", "WARN", "ERROR"},
		MinLevel: logutils.LogLevel("INFO"),
		Writer:   os.Stdout,
	}

	log.SetFlags(log.Ldate | log.Ltime)

	if dbg {
		log.SetFlags(log.Ldate | log.Ltime | log.Lmicroseconds | log.Lshortfile)
		filter.MinLevel = logutils.LogLevel("DEBUG")
	}
	log.SetOutput(filter)
}

