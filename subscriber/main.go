package main

import (
	"flag"
	"github.com/hashicorp/logutils"
	"log"
	"net/url"
	"os"

	"github.com/gorilla/websocket"
)

var (
	addr = flag.String("addr", ":8080", "server address")
	dbg  = flag.Bool("debug", false, "debug mode")
)

func main() {
	flag.Parse()

	setupLog(*dbg)

	u := url.URL{Scheme: "ws", Host: *addr, Path: "/subscriber"}
	log.Printf("[INFO] connecting to server (%s)", u.String())

	c, _, err := websocket.DefaultDialer.Dial(u.String(), nil)
	if err != nil {
		log.Fatalf("[ERROR] can't get connection: %v", err)
	}
	defer c.Close()

	for {
		_, msg, err := c.ReadMessage()
		if err != nil {
			if websocket.IsUnexpectedCloseError(err, websocket.CloseGoingAway, websocket.CloseAbnormalClosure) {
				log.Printf("[ERROR] can't read message from publisher: %v\n", err)
			}
			break
		}
		log.Printf("[DEBUG] received: %s", msg)
	}

	log.Println("[WARN] connection lost")
}

func setupLog(dbg bool) {
	filter := &logutils.LevelFilter{
		Levels:   []logutils.LogLevel{"DEBUG", "INFO", "WARN", "ERROR"},
		MinLevel: logutils.LogLevel("INFO"),
		Writer:   os.Stdout,
	}

	log.SetFlags(log.Ldate | log.Ltime)

	if dbg {
		log.SetFlags(log.Ldate | log.Ltime | log.Lmicroseconds | log.Lshortfile)
		filter.MinLevel = logutils.LogLevel("DEBUG")
	}
	log.SetOutput(filter)
}
